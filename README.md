# Simpleton Analytics

<img align="left" height="100px" src="https://gitlab.com/viktomas/simpleton-analytics/-/raw/images/img/simpleton.svg" />

Simpleton is privacy-first, basic web analytics. It is an ansible playbook that will turn your Ubuntu server into an analytics server.

Simpleton is preconfigured [Nginx](https://www.nginx.com/) and [GoAccess](https://goaccess.io/) combination. After the installation, you can see your stats on demand by running a simple command.

---

![GoAccess example screenshot](https://gitlab.com/viktomas/simpleton-analytics/-/raw/images/img/small-screenshot.png)

## Main features

- **Privacy focused** - no cookies, no session tracking, you only know which pages got visited
- Not blocked by AdBlockers (yet). [AdBlockers hide majority of your visitors.](https://blog.viktomas.com/posts/adblock-skews-analytics/)
- Lightweight
- Quick to setup
- Fully automated
- Open-source
- Cheap and in your control

## How does it work?

```mermaid
graph LR
A["your website"] --"https tracking calls"--> B
subgraph Simpleton server
B["Nginx"] --> C
C["logfiles"] --> D
D["GoAccess"]
end
D --> E["analytics report"]
```

You add a simple `fetch()` call to your website. Your website then makes a request to simpleton server on each page load and Nginx stores the page view as a log entry. Then when you call [the report command](#get-your-statistics), simpleton calls `goaccess` and fetches the report to your computer.

## Prepare for the installation

Make sure you have done the following steps before installing simpleton.

### Your machine

The only requirement is to have [ansible installed](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html). I've worked with `2.8.0`, but any recent version should be fine.

### The analytics server

**Warning: If you already use the server for some other purpose you must look at the firewall and Nginx setup to make sure simpleton is not going to break your current apps.**

1. The server needs to be a Ubuntu server (tested on 18.04 and 20.04). Ideally, the server contains a fresh OS installation. I've worked with Digital Ocean $5 droplet.
1. Pont a DNS A record to your server (so you can get Let's Encrypt certificate) (validate with `dig analytics.example.com`)
1. You have to be able to SSH into the server (`ssh root@analytics.example.com`)
1. Your user has to have root privileges

## Install simpleton

1. put your own hostname in the [`hosts`](hosts) file
1. configure simpleton in [`group_vars/all`](group_vars/all)
    - `domain_name` - **required** - your analytics server host name (same as DNS A record)
    - `monitored_site` - **required** - the site that you are monitoring with this analytics, set as [`Access-Control-Allow-Origin` header](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin)
    - `contact_email` - **required** - Let's Encrypt will communicate any issues to this email
    - `log_file` - *optional* - how are the analytics logs named on the server
1. install simpleton with

    ```sh
    ansible-playbook site.yml
    ```

## Test your installation

Now you can run `curl https://analytics.example.com`, and you should see `pong` as a response

## Include the analytics snippet on your site

Add this snippet to your site:

```html
<script>
  fetch('https://analytics.example.com/ping'+location.pathname, { headers: { 'X-Referrer': document.referrer||''}});
</script>
```

## Get your statistics

You can generate an analytics report at any time by running:

```sh
ansible-playbook -t goaccess site.yml
```

## Update nginx config

You can synchronise only the `roles/nginx/templates/default.j2` and `roles/nginx/templates/nginx.conf` by running:

```sh
ansible-playbook -t nginx site.yml
```

## Built with

- [Ansible](https://www.ansible.com/)
- [Nginx](https://www.nginx.com/)
- [GoAccess](https://goaccess.io/)
- [Let's Encrypt](https://letsencrypt.org/)
- [github.com/coopdevs/certbot_nginx/](https://github.com/coopdevs/certbot_nginx/) for Let's Encrypt configuration
